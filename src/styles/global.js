import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    :root {
        --blue: #1480fb;
        --indigo: #007aff;
        --purple: #01356c;
        --white: #f9f9f9;
        --darkBlue: #05133d;
        --gray: #666360;
        --red: #c53030;
    }

    body{
        background: var(--white);
        color: var(--darkBlue);
    }
    body,button{
        font-family: "Montserrat", sans-serif;
        font-size: 1rem;
    }

    button{
        background-color: var(--indigo);
        color: var(--white);
        width: 100px;
        height: 40px;
        cursor: pointer;
        border: none;
        border-radius: 4px;
    }
    a{
        text-decoration: none;
    }
`;

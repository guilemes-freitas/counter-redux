import { useDispatch, useSelector } from "react-redux";
import {
  operateNumber,
  multiplyNumber,
} from "../../store/modules/counter/actions";
import { Container, CounterH1, ButtonsContainer } from "./styles";

const Counter = () => {
  const counter = useSelector((state) => state.counter);
  const dispatch = useDispatch();
  const handleNumber = (number) => {
    dispatch(operateNumber(number));
  };
  const handleMultiply = () => {
    dispatch(multiplyNumber());
  };
  return (
    <Container>
      <CounterH1>{counter}</CounterH1>
      <ButtonsContainer>
        <button
          onClick={() => {
            handleNumber(1);
          }}
        >
          +1
        </button>
        <button
          onClick={() => {
            handleNumber(10);
          }}
        >
          +10
        </button>
        <button
          onClick={() => {
            handleNumber(-10);
          }}
        >
          -10
        </button>
        <button
          onClick={() => {
            handleNumber(-1);
          }}
        >
          -1
        </button>
        <button onClick={handleMultiply}>x2</button>
      </ButtonsContainer>
    </Container>
  );
};

export default Counter;

import styled from "styled-components";
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: var(--white);
  width: 100vw;
  height: 100vh;
  button {
    transition: 0.5s;
  }
  button + button {
    margin-left: 2rem;
  }
  button:hover {
    background-color: var(--red);
  }
`;

export const CounterH1 = styled.h1`
  color: var(--darkBlue);
  font-family: "Roboto Mono", monospace;
  transition: 0.5s;

  :hover {
    color: var(--red);
  }
`;

export const ButtonsContainer = styled.div`
  margin-top: 2rem;
  display: flex;
`;

import { OPERATE_NUMBER, MULTIPLY_NUMBER } from "./actionsTypes";

export const operateNumber = (number) => ({
  type: OPERATE_NUMBER,
  number,
});

export const multiplyNumber = () => ({
  type: MULTIPLY_NUMBER,
});

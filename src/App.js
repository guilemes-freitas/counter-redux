import Counter from "./components/Counter";
import GlobalStyle from "./styles/global";

function App() {
  return (
    <>
      <GlobalStyle />
      <div className="App">
        <Counter></Counter>
      </div>
    </>
  );
}

export default App;

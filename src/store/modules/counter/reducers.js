import { OPERATE_NUMBER, MULTIPLY_NUMBER } from "./actionsTypes";

const reducerCounter = (state = 0, action) => {
  switch (action.type) {
    case OPERATE_NUMBER:
      const { number } = action;
      return state + number;
    case MULTIPLY_NUMBER:
      return state * 2;
    default:
      return state;
  }
};

export default reducerCounter;
